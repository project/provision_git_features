<?php

/**
 * @file
 * Provision/Drush hooks for git commands.
 */

/**
 * Implementation of hook_drush_command().
 */
function provision_git_features_drush_command() {
  $items['provision-commit'] = array(
    'description' => 'Export the site\'s Features and commit the result.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
      'message' => 'The commit message.'
    ),
    'aliases' => array('pc'),
  );
  return $items;
}

/**
 * Pre provision-git-features-update-commit hook
 */
function drush_provision_pre_provision_git_features_update_all_commit(){
  provision_git_is_repo();
}


/**
 * Implements the provision-git-pull command.
 */
function drush_provision_git_features_update_all_commit() {

  $site_path = d()->platform->root;
  $target = d()->name;
  
  //Pause Hostmaster (Mainly for the git pull task)
  // @TODO Better way?  @see drush_hosting_pause()
  exec('crontab -r');
  
  // Update All Features
  provision_backend_invoke($target, 'features-update-all');

  //Execute git commit
  //Generate commit message
  $message = array();
  $message[] = "Exported from: $target";
  $message[] = str_repeat('-', strlen($message[0]));
  if (drush_get_option('message')){
    $message[] = drush_get_option('message');
  }
  $message = implode("\n", $message);

  $data = array(
    'message' => $message,
  );
  
  //Invoke provision-git-commit
  provision_backend_invoke($target, 'provision-git-commit', array(), $data);
  
  //Push, if the user wants.
  //@TODO: Run this from hostmaster
  //if (drush_get_option('push')){
    provision_backend_invoke($target, 'provision-git-push');
  //}
  
  //Resume cron
  // @TODO: how to get hostmaster site name?  @see drush_provision_post_hostmaster_migrate()
  // they use drush_get_option('site_name')
  // @TODO Better way?  @see drush_hosting_pause()
  provision_backend_invoke('hostmaster', 'hosting-setup');
}